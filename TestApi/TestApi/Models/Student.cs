﻿namespace TestApi.Models
{
    public class Student
    {
        public long id { get; set; }

        public string? firstName { get; set; }

        public int age { get; set; }
    }
}
