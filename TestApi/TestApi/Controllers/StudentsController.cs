using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestApi.Models;

namespace TestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly StudentContext _context;

        public StudentsController(StudentContext context)
        {
            _context = context;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<Student>>> GetStudentItems()
        {
            return await _context.students.ToListAsync();
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<Student>> GetStudentItem(long id)
        {
            var student = await _context.students.FindAsync(id);

            if (student == null)
            {
                return NotFound();
            }

            return student;
        }


        [HttpPost]
        public async Task<ActionResult<Student>> PostTodoItem(Student student)
        {
            _context.students.Add(student);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetStudentItem), new { id = student.id }, student);
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem(long id, Student student)
        {
            if (id != student.id)
            {
                return BadRequest();
            }

            _context.Entry(student).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStudentItem(long id)
        {
            var studentItem = await _context.students.FindAsync(id);
            if (studentItem == null)
            {
                return NotFound();
            }

            _context.students.Remove(studentItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool StudentItemExists(long id)
        {
            return _context.students.Any(x => x.id == id);
        }
    }
}
